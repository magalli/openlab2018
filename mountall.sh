#!/bin/sh

mount_exp() {
  for i in {0..23}; do
    ii=$(printf '%02d' $i)
    mount /dev/disk/by-path/*-sas-$1-phy$i-lun-0-part1 /mnt/e${2}d$ii
  done
}

mount_exp exp0x50001555e2b3623f 00
mount_exp exp0x50001555e2ef023f 01
mount_exp exp0x50001555e132023f 02
mount_exp exp0x50001555e2e4e23f 03
