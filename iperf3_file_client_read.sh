#!/bin/sh

port=5100

for d in /mnt/*/test
do
  iperf3 -c 192.168.254.1 -p $port -t 10000 -F $d &
  (( port++ ))
done

