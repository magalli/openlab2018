#!/bin/sh

if [[ $2 == all ]]
then
  for d in /mnt/*
  do
    sudo ./$1_disk client 192.168.254.1 6000 $d/test &
  done
elif [[ $2 == 'custom' ]]
then
  if [[ $3 == 'single' ]]
  then
    for d in /mnt/e00*
    do
      sudo ./$1_disk client 192.168.254.1 6000 $d/test &
    done
  elif [[ $3 == 'double_1' ]]
  then
    for b in {0,1}
    do
      for d in /mnt/e0$b*
      do
        sudo ./$1_disk client 192.168.254.1 6000 $d/test &
      done
    done
  elif [[ $3 == 'double_2' ]]
  then
    for b in {2,3}
    do
      for d in /mnt/e0$b*
      do
        sudo ./$1_disk client 192.168.254.1 6000 $d/test &
      done
    done
  else
   echo "Madonna troia"
 fi
else
  echo "First argument has to be read_from or write_to, second can be all or custom; always execute on the client machine"
fi

