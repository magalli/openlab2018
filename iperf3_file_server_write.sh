#!/bin/sh

port=5100

for d in /mnt/*/test
do
  sudo iperf3 -s -p $port -F $d &
  (( port++ ))
done

