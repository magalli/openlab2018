#!/bin/sh

for box in {00..03}
do
  for disk in {00..23}
  do
    sudo dd of=/mnt/e$boxd$disk if=/dev/zero bs=16M oflag=direct &
  done
done

