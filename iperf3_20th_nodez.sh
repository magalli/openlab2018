#!/bin/sh

taskset 00,00000001 iperf3 -s -p 5100 &
taskset 00,00000002 iperf3 -s -p 5101 &
taskset 00,00000004 iperf3 -s -p 5102 &
taskset 00,00000008 iperf3 -s -p 5103 &
taskset 00,00000010 iperf3 -s -p 5104 &
taskset 00,00000020 iperf3 -s -p 5105 &
taskset 00,00000040 iperf3 -s -p 5106 &
taskset 00,00000080 iperf3 -s -p 5107 &
taskset 00,00000100 iperf3 -s -p 5108 &
taskset 00,00000200 iperf3 -s -p 5109 &
taskset 00,00100000 iperf3 -s -p 5110 &
taskset 00,00200000 iperf3 -s -p 5111 &
taskset 00,00400000 iperf3 -s -p 5112 &
taskset 00,00800000 iperf3 -s -p 5113 &
taskset 00,01000000 iperf3 -s -p 5114 &
taskset 00,02000000 iperf3 -s -p 5115 &
taskset 00,04000000 iperf3 -s -p 5116 &
taskset 00,08000000 iperf3 -s -p 5117 &
taskset 00,10000000 iperf3 -s -p 5118 &
taskset 00,20000000 iperf3 -s -p 5119 &
