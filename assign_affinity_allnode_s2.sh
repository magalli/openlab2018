#!/bin/sh
# assign irqs 203-242 to CPUs 0-9, 20-29

i=203

while [ $i -le 242 ]
do
  echo 00,00000001 > /proc/irq/$i/smp_affinity
  echo 00,00000002 > /proc/irq/$((i+1))/smp_affinity
  echo 00,00000004 > /proc//irq/$((i+2))/smp_affinity
  echo 00,00000008 > /proc/irq/$((i+3))/smp_affinity
  echo 00,00000010 > /proc/irq/$((i+4))/smp_affinity
  echo 00,00000020 > /proc/irq/$((i+5))/smp_affinity
  echo 00,00000040 > /proc/irq/$((i+6))/smp_affinity
  echo 00,00000080 > /proc/irq/$((i+7))/smp_affinity
  echo 00,00000100 > /proc/irq/$((i+8))/smp_affinity
  echo 00,00000200 > /proc/irq/$((i+9))/smp_affinity

  echo 00,00100000 > /proc/irq/$((i+10))/smp_affinity
  echo 00,00200000 > /proc/irq/$((i+11))/smp_affinity
  echo 00,00400000 > /proc/irq/$((i+12))/smp_affinity
  echo 00,00800000 > /proc/irq/$((i+13))/smp_affinity
  echo 00,01000000 > /proc/irq/$((i+14))/smp_affinity
  echo 00,02000000 > /proc/irq/$((i+15))/smp_affinity
  echo 00,04000000 > /proc/irq/$((i+16))/smp_affinity
  echo 00,08000000 > /proc/irq/$((i+17))/smp_affinity
  echo 00,10000000 > /proc/irq/$((i+18))/smp_affinity
  echo 00,20000000 > /proc/irq/$((i+19))/smp_affinity


  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
done
