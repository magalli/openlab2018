#!/bin/sh
# assign packets coming from ports 5000-5019 directed to host 192.168.254.1 to RX 0-19

queue=0

for port in {00..19}
do
 sudo ethtool -N eth4 flow-type tcp4 dst-ip 192.168.254.1 src-port 50$port action $queue
 (( queue++ ))
done



