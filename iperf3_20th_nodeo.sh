#!/bin/sh

taskset 00,00000400 iperf3 -s -p 5100 &
taskset 00,00000800 iperf3 -s -p 5101 &
taskset 00,00001000 iperf3 -s -p 5102 &
taskset 00,00002000 iperf3 -s -p 5103 &
taskset 00,00004000 iperf3 -s -p 5104 &
taskset 00,00008000 iperf3 -s -p 5105 &
taskset 00,00010000 iperf3 -s -p 5106 &
taskset 00,00020000 iperf3 -s -p 5107 &
taskset 00,00040000 iperf3 -s -p 5108 &
taskset 00,00080000 iperf3 -s -p 5109 &
taskset 00,40000000 iperf3 -s -p 5110 &
taskset 00,80000000 iperf3 -s -p 5111 &
taskset 01,00000000 iperf3 -s -p 5112 &
taskset 02,00000000 iperf3 -s -p 5113 &
taskset 04,00000000 iperf3 -s -p 5114 &
taskset 08,00000000 iperf3 -s -p 5115 &
taskset 10,00000000 iperf3 -s -p 5116 &
taskset 20,00000000 iperf3 -s -p 5117 &
taskset 40,00000000 iperf3 -s -p 5118 &
taskset 80,00000000 iperf3 -s -p 5119 &
