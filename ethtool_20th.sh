#!/bin/sh
# assign packets with destination ports 5100-5119 to RXs 0-19

queue=0

for port in {00..19}
do
 sudo ethtool -N eth4 flow-type tcp4 dst-port 51$port action $queue
 (( queue++ ))
done



