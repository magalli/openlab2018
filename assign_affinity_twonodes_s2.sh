#!/bin/sh
# assign irqs 203-242 to CPUs 0-39

i=203

echo 00,00000001 > /proc/irq/$i/smp_affinity
echo 00,00000002 > /proc/irq/$((i+1))/smp_affinity
echo 00,00000004 > /proc//irq/$((i+2))/smp_affinity
echo 00,00000008 > /proc/irq/$((i+3))/smp_affinity
echo 00,00000010 > /proc/irq/$((i+4))/smp_affinity
echo 00,00000020 > /proc/irq/$((i+5))/smp_affinity
echo 00,00000040 > /proc/irq/$((i+6))/smp_affinity
echo 00,00000080 > /proc/irq/$((i+7))/smp_affinity
echo 00,00000100 > /proc/irq/$((i+8))/smp_affinity
echo 00,00000200 > /proc/irq/$((i+9))/smp_affinity

echo 00,00100000 > /proc/irq/$((i+10))/smp_affinity
echo 00,00200000 > /proc/irq/$((i+11))/smp_affinity
echo 00,00400000 > /proc/irq/$((i+12))/smp_affinity
echo 00,00800000 > /proc/irq/$((i+13))/smp_affinity
echo 00,01000000 > /proc/irq/$((i+14))/smp_affinity
echo 00,02000000 > /proc/irq/$((i+15))/smp_affinity
echo 00,04000000 > /proc/irq/$((i+16))/smp_affinity
echo 00,08000000 > /proc/irq/$((i+17))/smp_affinity
echo 00,10000000 > /proc/irq/$((i+18))/smp_affinity
echo 00,20000000 > /proc/irq/$((i+19))/smp_affinity

echo 00,00000400 > /proc/irq/$((i+20))/smp_affinity
echo 00,00000800 > /proc/irq/$((i+21))/smp_affinity
echo 00,00001000 > /proc//irq/$((i+22))/smp_affinity
echo 00,00002000 > /proc/irq/$((i+23))/smp_affinity
echo 00,00004000 > /proc/irq/$((i+24))/smp_affinity
echo 00,00008000 > /proc/irq/$((i+25))/smp_affinity
echo 00,00010000 > /proc/irq/$((i+26))/smp_affinity
echo 00,00020000 > /proc/irq/$((i+27))/smp_affinity
echo 00,00040000 > /proc/irq/$((i+28))/smp_affinity
echo 00,00080000 > /proc/irq/$((i+29))/smp_affinity

echo 00,40000000 > /proc/irq/$((i+30))/smp_affinity
echo 00,80000000 > /proc/irq/$((i+31))/smp_affinity
echo 01,00000000 > /proc/irq/$((i+32))/smp_affinity
echo 02,00000000 > /proc/irq/$((i+33))/smp_affinity
echo 04,00000000 > /proc/irq/$((i+34))/smp_affinity
echo 08,00000000 > /proc/irq/$((i+35))/smp_affinity
echo 10,00000000 > /proc/irq/$((i+36))/smp_affinity
echo 20,00000000 > /proc/irq/$((i+37))/smp_affinity
echo 40,00000000 > /proc/irq/$((i+38))/smp_affinity
echo 80,00000000 > /proc/irq/$((i+39))/smp_affinity

