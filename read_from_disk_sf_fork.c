#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/sendfile.h>

void error(char *msg)
{
  perror(msg);
  exit(1);
}

int main (int argc, char *argv[]) {

  int sockfd, portno, file;
  char file_name[256];
  const size_t buffer_len = 1024 * 1024;
  char* buffer = malloc(sizeof(char[buffer_len]));
  if(buffer == NULL)
  {
    printf("Memory is not enough\n");
    exit(1);
  }
  struct sockaddr_in serv_addr, cli_addr;
  ssize_t bytes_trans = buffer_len;
  ssize_t bytes_read = buffer_len;
  ssize_t bytes_written = buffer_len;
  ssize_t msg;
  off_t offset = 0;

  if (strcmp( argv[1], "client") == 0) {

    struct hostent *server;
    int pid;

    if (argc < 4) {
      fprintf(stderr,"usage %s: mode, hostname, port\n", argv[0]);
      exit(0);
    }
    portno = atoi(argv[3]);

    int e;
    for (e=0; e<4; e++){
      int d;
      for (d=0; d<10; d++) {
        pid = fork();
          if (pid<0) error("ERROR forking");
        if (pid == 0) {
          sprintf(file_name,"/mnt/e0%id0%i/test",e,d);
          printf("%s\n",file_name);
          sockfd = socket(AF_INET, SOCK_STREAM, 0);
          if (sockfd < 0) error("ERROR opening socket");
          server = gethostbyname(argv[2]);
          if (server == NULL) {
            fprintf(stderr,"ERROR, no such host\n");
            exit(0);
          }
          serv_addr.sin_family = AF_INET;
          bcopy((char *)server->h_addr,
              (char *)&serv_addr.sin_addr.s_addr,
              server->h_length);
          serv_addr.sin_port = htons(portno);
          if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) error("ERROR connecting");
          file = open(file_name, O_RDONLY);
          if (file<0) error("error opening file");
          while (1) {
            bytes_trans = sendfile(sockfd,file,&offset,buffer_len);
            if (bytes_trans == -1) error("sendfile error");
          }
          close(sockfd);
          close(file);
        }
      }
      for (d=10; d<24; d++) {
        pid = fork();
          if (pid<0) error("ERROR forking");
        if (pid == 0) {
          sprintf(file_name,"/mnt/e0%id%i/test",e,d);
          printf("%s\n",file_name);
          sockfd = socket(AF_INET, SOCK_STREAM, 0);
          if (sockfd < 0) error("ERROR opening socket");
          server = gethostbyname(argv[2]);
          if (server == NULL) {
            fprintf(stderr,"ERROR, no such host\n");
            exit(0);
          }
          serv_addr.sin_family = AF_INET;
          bcopy((char *)server->h_addr,
              (char *)&serv_addr.sin_addr.s_addr,
              server->h_length);
          serv_addr.sin_port = htons(portno);
          if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) error("ERROR connecting");
          file = open(file_name, O_RDONLY);
          if (file<0) error("error opening file");
          while (1) {
            bytes_trans = sendfile(sockfd,file,&offset,buffer_len);
            if (bytes_trans == -1) error("sendfile error");
          }
          close(sockfd);
          close(file);
        }
      }
    }
  }

  else if (strcmp( argv[1], "server") == 0) {

    int newsockfd;
    socklen_t clilen;
    int pid;
    size_t tot_bytes_read;
    size_t tot_bytes_written;

    if (argc < 3) {
      fprintf(stderr,"usage: mode, port\n");
      exit(1);
    }
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) error("ERROR opening socket");
    portno = atoi(argv[2]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) error("ERROR on binding");
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
    // multiple clients
    while (1) {
      newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
      if (newsockfd < 0) error("ERROR on accept");
      //fork new process
      pid = fork();
      printf("pid: %i\n",pid);
      if (pid < 0) {
        error("ERROR in new process creation");
      }
      if (pid == 0) {
        //child process
        close(sockfd);
        sprintf(file_name,"/dev/null");
        file = open(file_name, O_WRONLY, S_IRUSR | S_IWUSR);
        if (file<0){
          error("open error");
        }
        while (1){
          bytes_read = read(newsockfd,buffer,buffer_len); // read from socket to buffer
          if (bytes_read < 0) error("ERROR reading from buffer");
          bytes_written  = write(file,buffer,bytes_read); // write from buffer to /dev/null
          if (bytes_written < 0) error("ERROR writing to socket");

        }
        msg = write(newsockfd,"OK",2);
        if (msg <0) error("nothing written");
        close(newsockfd);
        return 0;
      }
      else {
        //parent process
        close(newsockfd);
      }
    }
  }

  else printf("No mode provided\n");

  return 0;
}
