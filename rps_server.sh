#!/bin/sh
# assigns rps affinities to CPUs 0-9, 20-29

i=0

while [ $i -le 39 ]
do
  echo 00,00000001 > /sys/class/net/eth4/queues/rx-$i/rps_cpus
  echo 00,00000002 > /sys/class/net/eth4/queues/rx-$((i+1))/rps_cpus
  echo 00,00000004 > /sys/class/net/eth4/queues/rx-$((i+2))/rps_cpus
  echo 00,00000008 > /sys/class/net/eth4/queues/rx-$((i+3))/rps_cpus
  echo 00,00000010 > /sys/class/net/eth4/queues/rx-$((i+4))/rps_cpus
  echo 00,00000020 > /sys/class/net/eth4/queues/rx-$((i+5))/rps_cpus
  echo 00,00000040 > /sys/class/net/eth4/queues/rx-$((i+6))/rps_cpus
  echo 00,00000080 > /sys/class/net/eth4/queues/rx-$((i+7))/rps_cpus
  echo 00,00000100 > /sys/class/net/eth4/queues/rx-$((i+8))/rps_cpus
  echo 00,00000200 > /sys/class/net/eth4/queues/rx-$((i+9))/rps_cpus

  echo 00,00100000 > /sys/class/net/eth4/queues/rx-$((i+10))/rps_cpus
  echo 00,00200000 > /sys/class/net/eth4/queues/rx-$((i+11))/rps_cpus
  echo 00,00400000 > /sys/class/net/eth4/queues/rx-$((i+12))/rps_cpus
  echo 00,00800000 > /sys/class/net/eth4/queues/rx-$((i+13))/rps_cpus
  echo 00,01000000 > /sys/class/net/eth4/queues/rx-$((i+14))/rps_cpus
  echo 00,02000000 > /sys/class/net/eth4/queues/rx-$((i+15))/rps_cpus
  echo 00,04000000 > /sys/class/net/eth4/queues/rx-$((i+16))/rps_cpus
  echo 00,08000000 > /sys/class/net/eth4/queues/rx-$((i+17))/rps_cpus
  echo 00,10000000 > /sys/class/net/eth4/queues/rx-$((i+18))/rps_cpus
  echo 00,20000000 > /sys/class/net/eth4/queues/rx-$((i+19))/rps_cpus



  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
done
