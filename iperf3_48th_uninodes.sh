#!/bin/sh

j=1

for i in {00..03}
do
  taskset 00,000000${j}0 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {04..07}
do
  taskset 00,00000${j}00 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {08..11}
do
  taskset 00,0000${j}000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {12..15}
do
  taskset 00,000${j}0000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {16..20}
do
  taskset 00,0${j}000000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {21..24}
do
  taskset 00,${j}0000000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {25..28}
do
  taskset 0${j},00000000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {29..32}
do
  taskset ${j}0,00000000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {33..36}
do
  taskset 00,000000${j}0 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {37..40}
do
  taskset 00,00000${j}00 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {41..44}
do
  taskset 00,0000${j}000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {45..47}
do
  taskset 00,000${j}0000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done


