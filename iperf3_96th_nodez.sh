#!/bin/sh

j=1

for i in {00..03}
do
  taskset 00,0000000${j} iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {04..07}
do
  taskset 00,000000${j}0 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {08..09}
do
  taskset 00,00000${j}00 iperf3 -s -p 51$i &
  if [[ $j == 2 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {10..13}
do
  taskset 00,00${j}00000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {14..17}
do
  taskset 00,0${j}000000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {18..19}
do
  taskset 00,${j}0000000 iperf3 -s -p 51$i &
  if [[ $j == 2 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done


for i in {20..23}
do
  taskset 00,0000000${j} iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {24..27}
do
  taskset 00,000000${j}0 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {28..29}
do
  taskset 00,00000${j}00 iperf3 -s -p 51$i &
  if [[ $j == 2 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {30..33}
do
  taskset 00,00${j}00000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {34..37}
do
  taskset 00,0${j}000000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {38..39}
do
  taskset 00,${j}0000000 iperf3 -s -p 51$i &
  if [[ $j == 2 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {40..43}
do
  taskset 00,0000000${j} iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {44..47}
do
  taskset 00,000000${j}0 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done


for i in {48..51}
do
  taskset 00,0000000${j} iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {52..55}
do
  taskset 00,000000${j}0 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {56..59}
do
  taskset 00,00000${j}00 iperf3 -s -p 51$i &
  if [[ $j == 2 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {60..63}
do
  taskset 00,00${j}00000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {64..67}
do
  taskset 00,0${j}000000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {68..71}
do
  taskset 00,${j}0000000 iperf3 -s -p 51$i &
  if [[ $j == 2 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done


for i in {72..75}
do
  taskset 00,0000000${j} iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {76..79}
do
  taskset 00,000000${j}0 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {80..83}
do
  taskset 00,00000${j}00 iperf3 -s -p 51$i &
  if [[ $j == 2 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {84..87}
do
  taskset 00,00${j}00000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {88..91}
do
  taskset 00,0${j}000000 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

for i in {92..95}
do
  taskset 00,${j}0000000 iperf3 -s -p 51$i &
  if [[ $j == 2 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

  taskset 00,00000001 iperf3 -s -p 5196 &



