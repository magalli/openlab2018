#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <features.h>
#include <errno.h>
#include <sys/sendfile.h>
#include <limits.h>
#include <aio.h>
#include <signal.h>

struct ioRequest {
        int wfd;
        struct aiocb *aiocbp;
};

void error(char *msg) {
        perror(msg);
        exit(1);
}

void setup_io(struct ioRequest *my_req, struct aiocb *my_aiocb, int fd, int fd_write, int opcode, size_t buf_size, char* buf, int ret) {
        /* Set up the AIO request */
        my_req->wfd = fd_write;
        my_req->aiocbp = my_aiocb;
        my_req->aiocbp->aio_fildes = fd;
        my_req->aiocbp->aio_lio_opcode = opcode; //LIO_READ, LIO_WRITE
        my_req->aiocbp->aio_buf = buf;
        my_req->aiocbp->aio_nbytes = buf_size;
        /* Link the AIO request with the Signal Handler */
        my_req->aiocbp->aio_sigevent.sigev_notify = SIGEV_SIGNAL;
        my_req->aiocbp->aio_sigevent.sigev_signo = SIGIO;
        my_req->aiocbp->aio_sigevent.sigev_value.sival_ptr = my_req;
        /* Initiate read or write request */
        if (opcode == LIO_READ) {
                //my_req->aiocbp->aio_offset = 0;
                ret = aio_read(my_req->aiocbp);
                if (ret == -1) error("aioread");
        }
        if (opcode == LIO_WRITE) {
                ret = aio_write(my_req->aiocbp);
                if (ret == -1) error("aiowrite");
        }
        return;
}

void aio_completion_handler(int signo, siginfo_t *info, void* context) {
        int ret;
        struct ioRequest *req;
        struct ioRequest *new_req;
        struct aiocb *new_aiocb;
        /* Ensure it's our signal */
        if (info->si_signo == SIGIO) {
                req = info->si_value.sival_ptr;
                /* Did the request complete? */
                if (aio_error(req->aiocbp) == 0) {
                        // Request completed successfully, get the return status/
                        if (req->aiocbp->aio_lio_opcode == LIO_READ) {
                                new_req = calloc(1, sizeof(struct ioRequest));
                                if (new_req == NULL) error("calloc");
                                new_aiocb = calloc(1, sizeof(struct aiocb));
                                if (new_aiocb == NULL) error("calloc");
                                setup_io(new_req, new_aiocb, req->wfd, req->aiocbp->aio_fildes, LIO_WRITE, req->aiocbp->aio_nbytes, req->aiocbp->aio_buf, ret);
                                setup_io(req, req->aiocbp, req->aiocbp->aio_fildes, req->wfd, LIO_READ, req->aiocbp->aio_nbytes, req->aiocbp->aio_buf, ret);
                        }
                        if (req->aiocbp->aio_lio_opcode == LIO_WRITE) {
                                setup_io(req, req->aiocbp, req->wfd, req->aiocbp->aio_fildes, LIO_READ, req->aiocbp->aio_nbytes, req->aiocbp->aio_buf, ret);
                                //free(req);
                                //free(req->aiocbp);
                        }
                }
                else {
                        // something happened/
                        printf("something happened\n");
                        int status;
                        int opcode = req->aiocbp->aio_lio_opcode;
                        //do {
                        //        status = aio_cancel(req->aiocbp->aio_fildes, req->aiocbp);
                        //        printf("status is %i\n",status);
                        //}
                        //while (status != AIO_CANCELED);
                        aio_cancel(req->aiocbp->aio_fildes, req->aiocbp);
                        setup_io(req, req->aiocbp, req->aiocbp->aio_fildes, req->wfd, opcode, req->aiocbp->aio_nbytes, req->aiocbp->aio_buf, ret);
                }
        }
        return;
}

void setup_handler (struct sigaction sig_act) {
        /* Set up the signal handler */
        sigemptyset(&sig_act.sa_mask);
        sig_act.sa_flags = SA_SIGINFO | SA_RESTART;
        sig_act.sa_sigaction = aio_completion_handler; //pointer to a signal catching function
        //sig_act.sa_handler = aio_completion_handler;
        if (sigaction(SIGIO, &sig_act, NULL) == -1) error("sigaction");
}

int main (int argc, char *argv[]) {

        int sockfd, portno, file, pid;
        char file_name[256];
        size_t buffer_len = 1024 * 1024;
        char *buffer = malloc(sizeof(char[buffer_len]));
        if(buffer == NULL) error("malloc");
        struct sockaddr_in serv_addr, cli_addr;
        ssize_t bytes_read;
        ssize_t bytes_written;
        ssize_t msg;

        if (strcmp( argv[1], "client") == 0) {

                struct hostent *server;
                int bytes_trans = 0;
                struct sigaction sa;

                if (argc < 6) {
                        fprintf(stderr,"usage %s: mode, hostname, port, number of disks, disk names\n", argv[0]);
                        exit(0);
                }
                portno = atoi(argv[3]);

                const int disksno = atoi(argv[4]);
                char file_names[disksno][256];
                int sockfd_arr[disksno];
                int disks[disksno];
                //printf("%i\n",disksno);

                struct ioRequest *local_req = calloc(disksno, sizeof(struct ioRequest));
                if(local_req == NULL) error("calloc");
                struct aiocb *local_aiocb = calloc(disksno, sizeof(struct aiocb));
                if(local_aiocb == NULL) error("calloc");
                char **cli_buf = calloc(disksno, sizeof(char[buffer_len]));
                if(cli_buf == NULL) error("calloc");

                int i;
                int fn_elem = 0;
                for (i=5; i<argc; i++) {
                        sprintf(file_names[fn_elem],argv[i]);
                        fn_elem++;
                }
                server = gethostbyname(argv[2]);
                if (server == NULL) {
                        fprintf(stderr,"ERROR, no such host\n");
                        exit(0);
                }
                serv_addr.sin_family = AF_INET;
                bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
                serv_addr.sin_port = htons(portno);
                // setup the signal handler
                setup_handler (sa);
                for (i=0; i<disksno; i++) {
                        // open socket
                        sockfd_arr[i] = socket(AF_INET, SOCK_STREAM, 0);
                        if (sockfd_arr[i] < 0) error("ERROR opening socket");
                        // connect socket
                        if (connect(sockfd_arr[i],(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) error("ERROR connecting");
                        // open disks
                        disks[i] = open(file_names[i], O_WRONLY, S_IRWXU | O_APPEND);
                        if (disks[i]<0) error("error opening file");
                        // initiate requests
                        setup_io(local_req, local_aiocb, sockfd_arr[i], disks[i], LIO_READ, buffer_len, buffer, bytes_trans);
                        ++local_req;
                        ++local_aiocb;
                        ++buffer;
                }
                for (;;) {
                        //        sleep(1);
                }
                close(sockfd);
                close(file);
        }

        else if (strcmp( argv[1], "server") == 0) {

                int newsockfd;
                socklen_t clilen;
                size_t tot_bytes_read;
                size_t tot_bytes_written;

                if (argc < 4) {
                        fprintf(stderr,"usage: mode, port, file to send\n");
                        exit(1);
                }
                sockfd = socket(AF_INET, SOCK_STREAM, 0);
                if (sockfd < 0) error("ERROR opening socket");
                portno = atoi(argv[2]);
                serv_addr.sin_family = AF_INET;
                serv_addr.sin_addr.s_addr = INADDR_ANY;
                serv_addr.sin_port = htons(portno);
                if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) error("ERROR on binding");
                listen(sockfd,5);
                clilen = sizeof(cli_addr);
                // multiple clients
                while (1) {
                        newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
                        if (newsockfd < 0) error("ERROR on accept");
                        //fork new process
                        pid = fork();
                        printf("pid: %i\n",pid);
                        if (pid < 0) {
                                error("ERROR in new process creation");
                        }
                        if (pid == 0) {
                                //child process
                                close(sockfd);
                                sprintf(file_name, argv[3]);
                                file = open(file_name, O_RDONLY);
                                if (file<0){
                                        error("open error");
                                }
                                while (1){
                                        bytes_read = read(file,buffer,buffer_len); // read from file to buffer
                                        if (bytes_read < 0) error("ERROR reading from buffer");
                                        bytes_written  = write(newsockfd,buffer,bytes_read); // write from buffer to newsocket
                                        if (bytes_written < 0) error("ERROR writing to socket");
                                }
                                msg = write(newsockfd,"OK",2);
                                if (msg <0) error("nothing written");
                                close(newsockfd);
                                return 0;
                        }
                        else {
                                //parent process
                                close(newsockfd);
                        }
                }
        }

        else printf("No mode provided\n");

        return 0;
}
