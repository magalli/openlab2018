#!/bin/sh

j=1

for i in {00..47}
do
  taskset 00,000000${j}0 iperf3 -s -p 51$i &
  if [[ $j == 8 ]]
  then
    j=1
  else
    j=$(( $j * 2 ))
  fi
done

