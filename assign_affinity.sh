#!/bin/sh
# assign IRQs 224-263 to CPUs 0,1,2,3
# use "for i in {224..263}; do sudo cat /proc/irq/$i/smp_affinity; done" to check the current affinities
# use "cat /proc/interrupts" to check the irq numbers you are interested in

i=224

while [ $i -le 263 ]
do
  echo 00,00000001 > /proc/irq/$i/smp_affinity
  echo 00,00000002 > /proc/irq/$((i+1))/smp_affinity
  echo 00,00000004 > /proc//irq/$((i+2))/smp_affinity
  echo 00,00000008 > /proc/irq/$((i+3))/smp_affinity
  (( i++ ))
  (( i++ ))
  (( i++ ))
  (( i++ ))
done
