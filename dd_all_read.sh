#!/bin/sh

for b in {00..03}
do
  for d in {00..23}
  do
    sudo dd if=/mnt/e$bd$d of=/dev/null bs=16M iflag=direct &
  done
done

