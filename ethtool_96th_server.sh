#!/bin/sh
# assign packages coming from ports 5000-5095 to RXs 0-39

queue=0

for port in {00..95}
do
 sudo ethtool -N eth4 flow-type tcp4 src-port 50$port action $queue
if [[ $queue == 39 ]]
then
queue=0
else
  (( queue++ ))
fi
done



